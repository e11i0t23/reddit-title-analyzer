import requests
import json
import time


words = {}
ids = {}
subreddits = {}

for i in range(0,8000):
    print("fetching new data set")
    response = requests.get("http://api.pushshift.io/reddit/search/submission?pretty&filter=id,title,subreddit&size=500")
    data = response.json()["data"]
    
      
    
    for i in data:
        SUB = i["subreddit"]
        if SUB in subreddits:
            subreddits[SUB]+=1
        else:
            subreddits[SUB] = 1
        ID = i["id"]
        if ID in ids:
            next
        else:
            ids[ID] = 1
            s = i["title"]
            wds = s.lower().split(" ")
            for x in wds:
                wd = ''.join(filter(str.isalnum, x))
                if wd in words:
                    words[wd]+=1
                else:
                    words[wd] = 1
    
    with open('result.json', 'w') as fp:
        json.dump(words, fp)
    with open('ids.json', 'w') as fd:
        json.dump(ids, fd)
    with open('subs.json', 'w') as fx:
        json.dump(subreddits, fx)
    print("Data Saved")
    time.sleep(3)

