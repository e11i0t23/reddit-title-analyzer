import json
import csv

def total(json):
    total = 0
    for data in json:
        total += json[data]
    return total

def percentJSON(json, total):
    newJSON = {}
    perctot = 0
    with open('words.csv', 'w') as csvfile:
        fieldnames  = ["word", "percent"]
        w = csv.DictWriter(csvfile, fieldnames=fieldnames)
        w.writeheader()
        for data in json:
            perc = float(int(f[data] / total * 10000000000)/100000000)
            newJSON[data] = perc
            perctot += perc
            w.writerow({"word":data.encode("utf-8"), "percent":perc})
    print(perctot)
    return newJSON

f = json.loads(open('SortedResults.json').read())
total = total(f)
perc = percentJSON(f, total)
with open('PercentResults.json', 'w') as fx:
    json.dump(perc, fx)